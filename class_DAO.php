<?php
  require_once "dbkoneksi.php";
    class DAO
    {
     private $tableName = "";
     private $koneksi = null ;

     public function __construct($table_name)
     {
       $this->tableName = $table_name;
       $database = new DBKoneksi();
       $this->koneksi = $database->getKoneksi();
     }

     public function getAll(){
       $sql = "SELECT * FROM " . $this->tableName;
       $ps = $this->koneksi->prepare($sql);
       $ps->execute();
       return $ps->fetchAll();
     }

     public function hapus($pk){
         $sql = "DELETE FROM " . $this->tableName . " WHERE id=?";
         $ps = $this->koneksi->prepare($sql);
         $ps->execute([$pk]);
         return $ps->rowCount(); // jika 1 sukses
     }

     public function findByID($pk){
       $sql = "SELECT * FROM " . $this->tableName . " WHERE id=?";
       $ps = $this->koneksi->prepare($sql);
       $ps->execute([$pk]);
       return $ps->fetch();
     }
}
