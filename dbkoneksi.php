<?php
class DBKoneksi{
    //deklarasikan variabel yg dibutuhkan
    private $host = 'localhost';
    private $user = 'siswa';
    private $pass = '123456';
    private $name = 'dbkegiatan';

    private $opsi = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ];

    private $koneksi = null;
    //buat koneksinya
    public function __construct()
    {
        $dsn = "mysql:host=".$this->host.";dbname=".$this->name;
        try{
            $this->koneksi = new PDO($dsn,
                $this->user,$this->pass,
                $this->opsi);

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
      public function getKoneksi()
    {
        return $this->koneksi;
    }
}

?>
