
<?php
include_once 'top.php';
require_once 'class_peserta.php';
?>
<h2>Daftar Kegiatan</h2>
<?php
 $obj_peserta = new peserta();
 $rows = $obj_peserta->getAll();
?>
<table class="table">
   <thead>
     <tr class="active">
       <th>No</th><th>nomor registrasi</th><th>nama</th><th>email</th><th>Action</th>
     </tr>
   </thead>
 <tbody>
 <?php
   $nomor = 1;
   foreach($rows as $row){
     echo '<tr><td>'.$nomor.'</td>';
     echo '<td>'.$row['nomor'].'</td>';
     echo '<td>'.$row['namalengkap'].'</td>';
     echo '<td>'.$row['email'].'</td>';
     echo '<td><a href="form_peserta.php?id='.$row['id']. '">Update</a></td>';
     echo '</tr>';
     $nomor++;
   }
 ?>
 </tbody>
</table>
<?php
include_once 'bottom.php';
?>
